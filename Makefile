SOURCES = $(ls src/mediawiki2latex-pyqt)
PREFIX ?= /usr
BINDIR = $(PREFIX)/bin
SHAREDIR = $(PREFIX)/share
MANDIR = $(SHAREDIR)/man
MAN1DIR = $(MANDIR)/man1
DESTDIR =
SUBDIRS = src

all: docs
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done

docs: mediawiki2latex-pyqt.1.gz

mediawiki2latex-pyqt.1.gz: mediawiki2latex-pyqt.1
	@gzip -c $< > $@

mediawiki2latex-pyqt.1: mediawiki2latex-pyqt.1.in
	@sed "s/MEDIAWIKI2LATEXPYQTVERSION/$(VERSION)/" $< > $@

clean:
	rm -f *~ mediawiki2latex-pyqt.1.gz
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done

install: all
	install -dm755 "$(DESTDIR)$(MAN1DIR)"
	install -m644 mediawiki2latex-pyqt.1.gz "$(DESTDIR)$(MAN1DIR)"
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done

.PHONY: all clean install
