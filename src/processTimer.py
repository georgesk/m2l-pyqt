licence={}
licence['en']="""\
processTimer.py version %s:

Defines a timer which will launch a process at every available opportunity

Copyright (C)2014 Georges Khaznadar <georgesk@debian.org>

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
<http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class processTimer(QTimer):
    """
    This class launches a process defined in a sequenceApp at each
    opportunity
    """
    def __init__(self, parent):
        """
        the constructor.
        @param parent a sequenceApp which maintains the process list and index
        """
        QTimer.__init__(self, parent)
        self.timeout.connect(self.idleProcess)

    def idleProcess(self):
        """
        callback for the timer.Starts a process in the list if
        one of them is to be started. Otherwise the application
        is closed, after a warning message.
        """
        self.parent().idleTick.emit()
            
