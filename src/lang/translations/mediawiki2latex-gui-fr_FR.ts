<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../mediawiki2latex-pyqt" line="5"/>
        <source>mediawiki2latex-pyqt version {version}:

a gui for mediawiki2latex

{authors}

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
&lt;http://www.gnu.org/licenses/&gt;.
</source>
        <translation type="obsolete">mediawiki2latex-pyqt version {version}:

une interface graphique pour mediawiki2latex

{authors}

Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou
le modifier sous les termes de la Licence Publique Générale GNU telle que
publiée par la Free Software Foundation, soit la version 2 de la
Licence, ou (à votre convenance) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il soit utile,
mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
VALEUR MARCHANDE ou POSSIBILITÉ D'UTILISER POUR UN BUT PARTICULIER.
Voyez la licence « GNU General Public License » pour plus de détails.

Vous devriez avoir reçu une copie de la « GNU General Public License »
avec ce programme, sinon voyez à &lt;http://www.gnu.org/licenses/&gt;.
</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../mediawiki2latex-pyqt" line="253"/>
        <source>mediawiki2latex-pyqt version {version}:

a gui for mediawiki2latex

{authors}

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
&lt;http://www.gnu.org/licenses/&gt;.
</source>
        <translation type="obsolete">mediawiki2latex-pyqt version {version}:

une interface graphique pour mediawiki2latex

{authors}

Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou
le modifier sous les termes de la Licence Publique Générale GNU telle que
publiée par la Free Software Foundation, soit la version 2 de la
Licence, ou (à votre convenance) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il soit utile,
mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
VALEUR MARCHANDE ou POSSIBILITÉ D'UTILISER POUR UN BUT PARTICULIER.
Voyez la licence « GNU General Public License » pour plus de détails.

Vous devriez avoir reçu une copie de la « GNU General Public License »
avec ce programme, sinon voyez à &lt;http://www.gnu.org/licenses/&gt;.
</translation>
    </message>
    <message>
        <location filename="../mediawiki2latex-pyqt" line="275"/>
        <source>French translation: (c) 2014 Georges Khaznadar &lt;georgesk@debian.org&gt;
</source>
        <translation type="obsolete">Traduction française : ©Georges Khaznadar &lt;georgesk@debian.org&gt; </translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../Ui_about.py" line="98"/>
        <source>About</source>
        <translation type="unfinished">À propos</translation>
    </message>
    <message>
        <location filename="../../Ui_about.py" line="99"/>
        <source>Authors</source>
        <translation type="unfinished">Auteurs</translation>
    </message>
    <message>
        <location filename="../../Ui_about.py" line="100"/>
        <source>License</source>
        <translation type="unfinished">Licence</translation>
    </message>
    <message>
        <location filename="../../Ui_about.py" line="101"/>
        <source>Translations</source>
        <translation type="unfinished">Traductions</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="72"/>
        <source>downloading article and contributor information</source>
        <translation>téléchargement des informations pour l'article et les contributeurs</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="74"/>
        <source>parsing article text</source>
        <translation>analyse du texte de l'article</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="76"/>
        <source>forking threads to download of images and contributor information on them</source>
        <translation>lancement de threads pour télécharger les images et les information légales qui s'y appliquent</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="78"/>
        <source>precompiling table columns</source>
        <translation>précompilation des colonnes de la table</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="80"/>
        <source>joining threads to download the images and contributor information on them</source>
        <translation>attente de la fin des threads pour télécharger les images et les information légales qui s'y appliquent</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="82"/>
        <source>preparing for PDF generation</source>
        <translation>préparation de la génération de PDF</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="84"/>
        <source>preparing images for LaTeX document</source>
        <translation>préparation des images pour le document LaTeX</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="86"/>
        <source>generating PDF file. LaTeX run 1 of 4</source>
        <translation>génération du fichier PDF. Compilation LaTeX 1/4</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="88"/>
        <source>generating PDF file. LaTeX run 2 of 4</source>
        <translation>génération du fichier PDF. Compilation LaTeX 2/4</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="90"/>
        <source>generating PDF file. LaTeX run 3 of 4</source>
        <translation>génération du fichier PDF. Compilation LaTeX 3/4</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="92"/>
        <source>generating PDF file. LaTeX run 4 of 4</source>
        <translation>génération du fichier PDF. Compilation LaTeX 4/4</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="94"/>
        <source>finished</source>
        <translation>terminé</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="201"/>
        <source>echo &apos;default message; you should not see it&apos;</source>
        <translation>echo 'message par défaut ; vous ne devriez pas le voir'</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="325"/>
        <source>File still open</source>
        <translation>Fichier encore ouvert</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="325"/>
        <source>Please close your PDF viewer before pressing the run button!
This program need writing permission on the PDF file!</source>
        <translation>Veuillez fermer votre visionneuse PDF avant d'appuyer sur le bouton de lancement !
Ce programme a besoin du droit d'écriture sur le fichier PDF !</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="334"/>
        <source>Mediawiki to Latex started...</source>
        <translation>Démarrage de Mediawiki to LaTeX ...</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="356"/>
        <source>MediaWiki</source>
        <translation>MediaWiki</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="357"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="360"/>
        <source>Rasterize</source>
        <translation>Rastériser</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="383"/>
        <source>running mediawiki2latex</source>
        <translation>mediawiki2latex lancé</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="406"/>
        <source>The sequence of processes is finished. You can close the window.</source>
        <translation>La séquences de procédures est terminée. Vous pouvez fermer la fenêtre.</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="184"/>
        <source>Mediawiki to LaTeX</source>
        <translation>Mediawiki to LaTeX</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="185"/>
        <source>Wiki source</source>
        <translation>Source dans le wiki</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="186"/>
        <source>Template expansion</source>
        <translation>Développement des modèles</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="187"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="188"/>
        <source>Mediawiki</source>
        <translation>Mediawiki</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="190"/>
        <source>Paper</source>
        <translation>Papier</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="191"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="192"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="193"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="194"/>
        <source>letter</source>
        <translation>letter</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="195"/>
        <source>legal</source>
        <translation>legal</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="196"/>
        <source>executive</source>
        <translation>executive</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="197"/>
        <source>Vector graphics</source>
        <translation>Graphismes vectoriels</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="199"/>
        <source>Keep vector form</source>
        <translation>Laisser en forme vectorielle</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="208"/>
        <source>LaTeX output</source>
        <translation type="obsolete">Sortie LaTeX</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="209"/>
        <source>excluded</source>
        <translation type="obsolete">exclue</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="205"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="206"/>
        <source>Logs</source>
        <translation>Journaux</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="207"/>
        <source>Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="208"/>
        <source>Run</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="214"/>
        <source>File</source>
        <translation type="obsolete">Fichier</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="215"/>
        <source>About</source>
        <translation type="obsolete">À propos</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="216"/>
        <source>Save preferences</source>
        <translation type="obsolete">Enregistrer les préférences</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="212"/>
        <source>&amp;Quit (Ctrl-Q)</source>
        <translation>&amp;Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="213"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="219"/>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <location filename="../Ui_MainWindow.py" line="220"/>
        <source>About ... (F1)</source>
        <translation type="obsolete">À propos ... (F1)</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="216"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="252"/>
        <source>mediawiki2latex-pyqt version {version}:

a gui for mediawiki2latex

{authors}

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
&lt;http://www.gnu.org/licenses/&gt;.
</source>
        <translation>mediawiki2latex-pyqt version {version}:

une interface graphique pour mediawiki2latex

{authors}

Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou
le modifier sous les termes de la Licence Publique Générale GNU telle que
publiée par la Free Software Foundation, soit la version 2 de la
Licence, ou (à votre convenance) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il soit utile,
mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
VALEUR MARCHANDE ou POSSIBILITÉ D'UTILISER POUR UN BUT PARTICULIER.
Voyez la licence « GNU General Public License » pour plus de détails.

Vous devriez avoir reçu une copie de la « GNU General Public License »
avec ce programme, sinon voyez à &lt;http://www.gnu.org/licenses/&gt;.
</translation>
    </message>
    <message>
        <location filename="../mediawiki2latex-pyqt" line="275"/>
        <source>French translation: (c) 2014 Georges Khaznadar &lt;georgesk@debian.org&gt;
</source>
        <translation type="obsolete">Traduction française : ©Georges Khaznadar &lt;georgesk@debian.org&gt; </translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="200"/>
        <source>Output Format</source>
        <translation>Format cible</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="201"/>
        <source>PDF</source>
        <translation>PDF</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="363"/>
        <source>LaTeX Source Zip</source>
        <translation>ZIP source LaTeX</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="209"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="210"/>
        <source>A&amp;bout</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="211"/>
        <source>&amp;Save preferences</source>
        <translation>Enregi&amp;strer les préférences</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="214"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../Ui_MainWindow.py" line="215"/>
        <source>&amp;About ... (F1)</source>
        <translation>À &amp;propos ... (F1)</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="364"/>
        <source>ODT (Word Processor)</source>
        <translation>ODT (traitement de texte)</translation>
    </message>
    <message>
        <location filename="../../mediawiki2latex-pyqt" line="365"/>
        <source>EPUB</source>
        <translation>EPUB</translation>
    </message>
</context>
</TS>
